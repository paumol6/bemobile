package com.bemobile.bemobileapp.utils

class Constants {
    companion object {
        const val ITEM_EXTRA = "item_extra"
        const val EURO_SUM = "euro_sum"
        const val EURO = "EUR"
        const val AUD = "AUD"
        const val CAD = "CAD"
        const val USD = "USD"
        const val BASE_URL = "https://quiet-stone-2094.herokuapp.com/"
    }
}