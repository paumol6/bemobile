package com.bemobile.bemobileapp.utils

import java.math.RoundingMode
import java.text.NumberFormat

class UtilsMoneyFormater {
    companion object {
        fun formatAmountDoubleToString(amount: Double?, currencyIso: String?, decimals: Int): String? {
            var currency: Currency = Currency.NONE
            try {
                if (currencyIso != null && currencyIso.length > 0) {
                    currency = Currency.valueOf(currencyIso)
                }
            } catch (e: IllegalArgumentException) {
              e.stackTrace
            }
            val formatter = NumberFormat.getInstance()
            formatter.maximumFractionDigits = decimals
            formatter.minimumFractionDigits = decimals
            formatter.roundingMode = RoundingMode.HALF_UP
            var amountString = formatter.format(amount)
            if ("NONE" == currencyIso || currencyIso == null || currencyIso.length == 0) {
                // Required for amount inputs (empty currencyIso)
                currency.setCurrencyString("")
            } else if (currency == Currency.NONE) {
                currency.setCurrencyString(currencyIso)
            }
            amountString = String.format(currency.formattedCurrency, amountString)
            return amountString
        }


        enum class Currency(private val amountString: String, private var currencyString: String) {
            NONE("%s", ""),
            EUR("%s", "€"),
            USD("%s", "$"),
            CAD("%s", "C$"),
            AUD("%s", "AUD");


            val formattedCurrency: String
                get() = amountString + currencyString

            fun setCurrencyString(currencyString: String) {
                this.currencyString = currencyString
            }

        }
    }
}