package com.bemobile.bemovileapp

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class BeMobileApp:Application()