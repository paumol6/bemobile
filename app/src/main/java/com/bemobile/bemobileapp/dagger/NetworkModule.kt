package com.bemobile.bemovileapp.di

import com.bemobile.bemobileapp.utils.Constants
import com.bemobile.bemovileapp.data.network.TradeApiClient
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    @Singleton
    @Provides
    fun provideRetrofit():Retrofit{
        return Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    @Singleton
    @Provides
    fun provideTradeApiClient(retrofit: Retrofit):TradeApiClient{
        return retrofit.create(TradeApiClient::class.java)
    }
}