package com.bemobile.bemovileapp.data

import com.bemobile.bemobileapp.data.model.RatesModel
import com.bemobile.bemovileapp.data.model.TradesModel
import com.bemobile.bemovileapp.data.model.TradesAndRatesProvider
import com.bemobile.bemovileapp.data.network.TradeService
import javax.inject.Inject

class TradeRepository @Inject constructor(
        private val api: TradeService,
        private val tradesAndRatesProvider: TradesAndRatesProvider
) {

    suspend fun getAllTrades(): ArrayList<TradesModel> {
        val response = api.getTrades()
        tradesAndRatesProvider.trades = response
        return response
    }

    suspend fun getAllRates(): ArrayList<RatesModel> {
        val response = api.getRates()
        tradesAndRatesProvider.rates = response
        return response
    }

}