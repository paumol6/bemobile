package com.bemobile.bemovileapp.data.network

import com.bemobile.bemobileapp.data.model.RatesModel
import com.bemobile.bemovileapp.data.model.TradesModel
import retrofit2.Response
import retrofit2.http.GET

interface TradeApiClient {
    @GET("transactions.json ")
    suspend fun getAllTrades(): Response<ArrayList<TradesModel>>

    @GET("rates.json ")
    suspend fun getAllRates(): Response<ArrayList<RatesModel>>
}