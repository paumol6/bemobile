package com.bemobile.bemovileapp.data.model

import com.bemobile.bemobileapp.data.model.RatesModel
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TradesAndRatesProvider @Inject constructor() {
    var trades: ArrayList<TradesModel> = ArrayList()
    var rates : ArrayList<RatesModel> = ArrayList()
}