package com.bemobile.bemobileapp.data.model

import com.google.gson.annotations.SerializedName

data class RatesModel (
    @SerializedName("rate") val rate: String,
    @SerializedName("to") val to: String,
    @SerializedName("from") val from: String
)