package com.bemobile.bemovileapp.data.network

import com.bemobile.bemobileapp.data.model.RatesModel
import com.bemobile.bemovileapp.data.model.TradesModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class TradeService @Inject constructor(private val api:TradeApiClient) {

    suspend fun getTrades(): ArrayList<TradesModel> {
        return withContext(Dispatchers.IO) {
            val response = api.getAllTrades()
            response.body()
        }!!
    }

    suspend fun getRates(): ArrayList<RatesModel> {
        return withContext(Dispatchers.IO) {
            val response = api.getAllRates()
            response.body()
        }!!
    }

}