package com.bemobile.bemovileapp.data.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class TradesModel(
    @SerializedName("sku") val sku: String,
    @SerializedName("amount") val amount: String,
    @SerializedName("currency") val currrency: String
) : Serializable