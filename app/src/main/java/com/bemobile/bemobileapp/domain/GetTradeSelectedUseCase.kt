package com.bemobile.bemovileapp.domain

import com.bemobile.bemobileapp.data.model.RatesModel
import com.bemobile.bemobileapp.utils.Constants
import com.bemobile.bemovileapp.data.model.TradesModel
import com.bemobile.bemovileapp.data.model.TradesAndRatesProvider
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

class GetTradeSelectedUseCase @Inject constructor(private val tradesAndRatesProvider: TradesAndRatesProvider) {

    public var euroValue: Double = 0.0
    var auxChange : Double = 0.0
    var ratesList : ArrayList<RatesModel> = ArrayList()

    var rateAUD = 0.0
    var rateCAD = 0.0
    var rateUSD = 0.0

    operator fun invoke( item : TradesModel): ArrayList<TradesModel>{
        val trades = tradesAndRatesProvider.trades
        var tradeListSelected : ArrayList<TradesModel> = ArrayList()
        if(!trades.isNullOrEmpty()){
            for ( trade : TradesModel in trades){
                if (trade.sku.equals(item.sku)){
                    euroValue+= conversionRates(trade.amount, trade.currrency)
                    tradeListSelected.add(trade)
                }
            }
        }
        return tradeListSelected
    }

    fun conversionRates(value: String, currency: String):  Double {
         ratesList = tradesAndRatesProvider.rates
        if (currency.equals(Constants.EURO)){
            return value.toDouble()
        } else {
           conversionValuesDiffEuro(value.toDouble(),currency)
        }
        return auxChange
    }

    fun conversionValuesDiffEuro(value: Double, currency: String): Double{
            when(currency){
                Constants.AUD -> return euroFromAUD(value)
                Constants.CAD -> return euroFromCAD(value)
                Constants.USD -> return euroFromUSD(value)
                else -> return value
            }
        }

    private fun euroFromUSD(value: Double):Double {
        if (rateUSD == 0.0){
            for (rate: RatesModel in ratesList) {
                if (rate.from.equals(Constants.USD)){
                    rateUSD = rate.rate.toDouble()
                }
            }
        }
        return euroFromAUD(value * rateAUD)
    }

    private fun euroFromCAD(value: Double): Double {
        if (rateCAD == 0.0){
            for (rate: RatesModel in ratesList) {
                if (rate.from.equals(Constants.CAD)){
                    rateCAD = rate.rate.toDouble()
                }
            }
        }
        return euroFromAUD(value * rateCAD)
    }


    fun euroFromAUD(value: Double) : Double {
        if (rateAUD == 0.0){
            for (rate: RatesModel in ratesList) {
                if (rate.from.equals(Constants.AUD)){
                    rateAUD = rate.rate.toDouble()
                }
            }
        }
        return value * rateAUD
    }
}