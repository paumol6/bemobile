package com.bemobile.bemovileapp.domain

import com.bemobile.bemovileapp.data.TradeRepository
import javax.inject.Inject

class GetTradeUseCase @Inject constructor(private val repository: TradeRepository) {
    suspend operator fun invoke() = repository.getAllTrades()
}