package com.bemobile.bemobileapp.domain

import com.bemobile.bemovileapp.data.TradeRepository
import javax.inject.Inject

class GetRateUseCase @Inject constructor(private val repository: TradeRepository) {
    suspend operator fun invoke() = repository.getAllRates()
}