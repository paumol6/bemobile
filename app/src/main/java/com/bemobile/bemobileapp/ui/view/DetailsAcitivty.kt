package com.bemobile.bemobileapp.ui.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.core.app.NotificationCompat.getExtras
import androidx.lifecycle.Observer
import com.bemobile.bemobileapp.data.model.RatesModel
import com.bemobile.bemobileapp.utils.Constants
import com.bemobile.bemobileapp.utils.UtilsMoneyFormater
import com.bemobile.bemovileapp.R
import com.bemobile.bemovileapp.data.model.TradesModel
import com.bemobile.bemovileapp.databinding.ActivityDetailsBinding
import com.bemobile.bemovileapp.ui.viewmodel.TradesViewModel

import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DetailsAcitivty : AppCompatActivity() {


    private lateinit var binding: ActivityDetailsBinding

    private val tradesViewModel: TradesViewModel by viewModels()

    lateinit var tradesModel: TradesModel
    var sumEuros : Double = 0.0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        getIntentExtras()
        configureView()

        configureObservers()
        tradesViewModel.getRates()
    }

    private fun getIntentExtras() {
        if (intent?.extras?.containsKey(Constants.ITEM_EXTRA)!!){
            tradesModel = (intent.getSerializableExtra(Constants.ITEM_EXTRA) as? TradesModel)!!
//            sumEuros = intent.getStringExtra(Constants.EURO_SUM).toString()
        }
    }

    private fun configureView() {
        binding.textViewAmount.text = UtilsMoneyFormater.formatAmountDoubleToString(sumEuros,UtilsMoneyFormater.Companion.Currency.EUR.toString(),2)
        binding.textViewName.text = tradesModel.sku
    }

    private fun configureObservers() {
        tradesViewModel.ratesModel.observe(this, Observer {
           tradesViewModel.selectedTrade(tradesModel)
            sumEuros = tradesViewModel.getEuroValue()
            configureView()
        })

    }
}