package com.bemobile.bemovileapp.ui.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.bemobile.bemobileapp.ui.view.DetailsAcitivty
import com.bemobile.bemobileapp.ui.view.adapter.CustomAdapter
import com.bemobile.bemobileapp.ui.view.listeners.OnClickItemInterface
import com.bemobile.bemobileapp.utils.Constants
import com.bemobile.bemovileapp.data.model.TradesModel
import com.bemobile.bemovileapp.databinding.ActivityMainBinding
import com.bemobile.bemovileapp.ui.viewmodel.TradesViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity(), OnClickItemInterface {

    private lateinit var binding: ActivityMainBinding

    private val tradesViewModel: TradesViewModel by viewModels()

    private var testServiceIncrement : Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        tradesViewModel.getTrades()
//        tradesViewModel.getRates()
        confifureObservers()
    }

    private fun confifureObservers() {
        tradesViewModel.tradeModel.observe(this, Observer {
            configureRecycle(tradesViewModel.tradeModelList)
        })

    }

    private fun configureRecycle(itemResponse: ArrayList<TradesModel>) {
        binding.tradesRecycleView?.layoutManager = LinearLayoutManager(this)
        var adapter = CustomAdapter(
            this@MainActivity,
            itemResponse,
            this@MainActivity
        )
        binding.tradesRecycleView?.adapter = adapter
    }

    override fun onClickItemDetail(item: TradesModel?) {
        if (item != null) {
             tradesViewModel.selectedTrade(item)
        }
        val intent = Intent(this@MainActivity, DetailsAcitivty::class.java)
        intent.putExtra(Constants.ITEM_EXTRA, item)
//        intent.putExtra(Constants.EURO_SUM,tradesViewModel.getEuroValue() )
        startActivity(intent)
    }

}