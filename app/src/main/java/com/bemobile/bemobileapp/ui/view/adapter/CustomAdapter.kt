package com.bemobile.bemobileapp.ui.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bemobile.bemobileapp.ui.view.listeners.OnClickItemInterface
import com.bemobile.bemobileapp.utils.UtilsMoneyFormater
import com.bemobile.bemovileapp.R
import com.bemobile.bemovileapp.data.model.TradesModel
import com.squareup.picasso.Picasso

class CustomAdapter(val context: Context, val items : ArrayList<TradesModel>, val listener : OnClickItemInterface) :
        RecyclerView.Adapter<CustomAdapter.ViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
                .inflate(R.layout.trades_layout_item, viewGroup, false)
        return ViewHolder(
            view
        )
    }


    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
       val tradesModel: TradesModel?  = items.get(position)
        viewHolder.bindItems(tradesModel,listener)
    }

    override fun getItemCount() = items.size

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun bindItems(item: TradesModel?, listener: OnClickItemInterface) {
            val textViewName = itemView.findViewById<TextView>(R.id.textViewName)
            val textViewAmount = itemView.findViewById<TextView>(R.id.textViewAmount)
            val textViewCurrency = itemView.findViewById<TextView>(R.id.textViewCurrency)

            val imageView = itemView.findViewById<ImageView>(R.id.imageViewItem)


            Picasso.with(itemView.context).load(R.drawable.gnb_logo)
                    .placeholder(R.mipmap.ic_launcher_round)// optional
                    .into(imageView)

            textViewName.text = item?.sku
            textViewAmount.text =  UtilsMoneyFormater.formatAmountDoubleToString(item?.amount?.toDouble(), item?.currrency,2)
            itemView.setOnClickListener { listener.onClickItemDetail(item) }
        }
    }

}


