package com.bemobile.bemovileapp.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.bemobile.bemobileapp.data.model.RatesModel
import com.bemobile.bemobileapp.domain.GetRateUseCase
import com.bemobile.bemobileapp.utils.Constants
import com.bemobile.bemovileapp.data.model.TradesModel
import com.bemobile.bemovileapp.domain.GetTradeUseCase
import com.bemobile.bemovileapp.domain.GetTradeSelectedUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import java.util.ArrayList
import javax.inject.Inject

@HiltViewModel
class TradesViewModel @Inject constructor(
        private val getTradeUseCase:GetTradeUseCase,
        private val getRateUseCase: GetRateUseCase,
        private val getTradeSelectedUseCase: GetTradeSelectedUseCase) : ViewModel() {

    val tradeModel = MutableLiveData<TradesModel>()
    val ratesModel = MutableLiveData<RatesModel>()
    val isLoading = MutableLiveData<Boolean>()

    var tradeModelList = ArrayList<TradesModel>()
    var rateModelList = ArrayList<RatesModel>()

    lateinit var euroValues : String

    fun getTrades() {
        viewModelScope.launch {
            isLoading.postValue(true)
            val result = getTradeUseCase()

            if(!result.isNullOrEmpty()){
                tradeModelList = result
                tradeModel.postValue(result[0])
            }
        }
    }

    fun getRates() {
        viewModelScope.launch {
            isLoading.postValue(true)
            val result = getRateUseCase()

            if(!result.isNullOrEmpty()){
                rateModelList = result
                ratesModel.postValue(result[0])
            }
        }
    }

    fun selectedTrade(tradeSelected : TradesModel) : ArrayList<TradesModel> {
        isLoading.postValue(true)
        val tradesSelected = getTradeSelectedUseCase(tradeSelected)
       return tradesSelected
    }

    fun getEuroValue(): Double{
        val number2digits =  getTradeSelectedUseCase.euroValue
        return number2digits
    }
}