package com.bemobile.bemobileapp.ui.view.listeners

import com.bemobile.bemovileapp.data.model.TradesModel

interface OnClickItemInterface {
    fun onClickItemDetail(item: TradesModel? )
}